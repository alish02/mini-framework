<?php

namespace App\Application;

use App\Application\Config\Config;
use App\Application\Router\Route;
use App\Application\Router\Router;
use App\Application\Views\View;
use App\Exceptions\ViewNotFound;

class App
{
    public function run(): void
    {
        try {
            $this->handle();
        } catch (ViewNotFound $e) {
            View::exception($e);
        }
    }

    private function handle()
    {
        Config::init(); // задача методы init в том что прочесть все файлы с конфигами
        $router = new Router();
        $router->handle(Route::list());
    }
}