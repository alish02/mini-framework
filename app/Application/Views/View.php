<?php

namespace App\Application\Views;

use App\Exceptions\ViewNotFound;

class View implements ViewInterface
{
    /**
     * @throws ViewNotFound
     */
    public static function show(string $view): void
    {
        $path = __DIR__ . "/../../../views/{$view}.view.php";
        if (!file_exists($path)) {
            throw new ViewNotFound("View ($view) not found");
        }
        include $path;
    }

    public static function exception(\Exception $e): void
    {
        extract([
            'message' => $e->getMessage(),  // блогодаря  extract доступна переменая $message в view
            'trace' => $e->getTraceAsString()
        ]);
        $config = include __DIR__ . '/../../../config/app.php';
        $path = __DIR__ . "/../../../views/{$config['exception_view']}.view.php";
        if (!file_exists($path)) {
            echo $e->getMessage();
            return; // выходим из функции
        }
        include $path;
    }
}