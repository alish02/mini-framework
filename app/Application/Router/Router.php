<?php

namespace App\Application\Router;

class Router implements RouterInterface
{
    public function handle(array $routes): void
    {
        foreach ($routes as $route) {
            if ($route['uri'] == $_SERVER['REQUEST_URI']) {
                $controller = new $route['controller']();
                $method = $route['method'];
                $controller->$method();
            }
        }
    }
}