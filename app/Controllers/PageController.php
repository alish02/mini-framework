<?php

namespace App\Controllers;

use App\Application\Views\View;
use App\Exceptions\ViewNotFound;

class PageController
{
    /**
     * @throws ViewNotFound
     */
    public function home(): void
    {
        View::show('pages/home');
    }

    /**
     * @throws ViewNotFound
     */
    public function about(): void
    {
        View::show('pages/about');
    }
}