<?php

use App\Application\Router\Route;
use App\Controllers\PageController;

Route::page("/home", PageController::class, 'home');
Route::page("/about", PageController::class, 'about');