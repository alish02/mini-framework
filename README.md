APP (функционал сайта):

    - application: (отвечает за то чтобы программа запускался и.т.д)
        - router (в этой директории находиться роуты)
            -Route.php (копит в себе все роуты)
            - Router.php (обрабатывает все роуты)
        - views (обработка views для роутов)
            - View.php (возращает нужный view для роута)
         - App.php (запуск сайта)

    - controllers: ()
        - PageController (регистрация роутов)
    
    - exceptios
        ViewNotFound (custom exception)